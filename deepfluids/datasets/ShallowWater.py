from torch.utils.data import Dataset
import os
import random
import numpy as np
import h5py

class ShallowWater(Dataset):
    def __init__(self,
                 root,
                 hyperparams,
                 keys,
                 transform=None):
        """
        """
        self.root = root
        self.hyperparams = hyperparams
        self.transform = transform
        self.keys = keys
        self.length = self.hyperparams.n_frames_in + self.hyperparams.n_frames_out + (self.hyperparams.n_frames_in + self.hyperparams.n_frames_out - 1)*self.hyperparams.n_frames_skip
        self._set_sequences()

        return

    def __len__(self):
        return len(self.sequences)

    def __getitem__(self, idx):

        # pick a random subsequence from the sequence
        sequence_start = random.randint(0, len(self.sequences[idx]) - self.length)

        return self.get_sequence(idx, sequence_start)

    def get_sequence(self, idx, sequence_start):
        
        sequence = self.sequences[idx]

        if sequence_start < 0 or sequence_start > len(sequence) - self.length:
            raise("Not valid time origin for sequence.") 
        
        # Compose the sample
        pos0 = sequence_start
        pos1 = sequence_start + self.hyperparams.n_frames_in*(1+self.hyperparams.n_frames_skip)
        pos2 = sequence_start + self.length
        sequence.open()
        sample = {"Input" : sequence[pos0:pos1:self.hyperparams.n_frames_skip+1],
                  "Output": sequence[pos1:pos2:self.hyperparams.n_frames_skip+1],
                  "Omega" : sequence.omega,
                  "h5file": sequence.h5file}
        sequence.close()

        # apply the transformations
        if self.transform:
            sample = self.transform(sample)

        return sample

    def get_complete_sequence(self, idx, sequence_start, sequence_finish):
        
        sequence = self.sequences[idx]

        if sequence_start < 0 or sequence_start > len(sequence) - self.length:
            raise("Not valid time origin for sequence.") 
        
        # Compose the sample
        pos0 = sequence_start
        pos1 = sequence_start + self.hyperparams.n_frames_in*(1+self.hyperparams.n_frames_skip)
        pos2 = sequence_start + self.length
        sequence.open()
        sample = {"Input" : sequence[pos0:pos1:self.hyperparams.n_frames_skip+1],
                  "Output": sequence[pos1:sequence_finish:self.hyperparams.n_frames_skip+1],
                  "Omega" : sequence.omega,
                  "h5file": sequence.h5file}
        sequence.close()

        # apply the transformations
        if self.transform:
            sample = self.transform(sample)

        return sample


    @property
    def transform(self):
        return self._transform

    @transform.setter
    def transform(self, value):
        self._transform = value

    @property
    def sequences(self):
        return self._sequences

    def _set_sequences(self):
        self._sequences = []

        folders = self.keys
        for folder in folders:
            simulation_folder = os.path.join(self.root, folder)
            sequences_paths = [os.path.join(self.root, folder, fol) for fol in sorted(os.listdir(simulation_folder))]

            for path in sequences_paths:
                sequence = Sequence(path)

                # discard sequences that have less frame than required
                if len(sequence) >= self.length:
                    self._sequences.append(sequence)
        return True


    # def close(self):
    #     for sequence in self.sequences:
    #         sequence.hf.close()


class Sequence(object):

    def __init__(self, h5file):
        self.h5file = h5file
        self.h = {}
        # Get length
        self.open()
        self.length = len(self.hf.keys())-1
        self.close()

    def open(self):
        self.hf = h5py.File(self.h5file, 'r')
        self.omega = np.array(self.hf.get("omega"))

    def close(self):
        self.hf.close()
        return 

    def __len__(self):
        return self.length

    def __getitem__(self, idx):
        # int case
        if type(idx) == int:
            if idx in self.h:
                return self.h[idx]

            h = np.array(self.hf.get("h"+str(idx)))
            self.h[idx] = h

            return h

        # slice case
        elif type(idx) == slice:
            hs = []
            start = idx.start or 0
            stop = idx.stop or len(self)
            step = idx.step or 1
            for i in range(start, stop, step):
                if i in self.h:
                    h = self.h[i]
                else:
                    h = np.array(self.hf.get("h"+str(i)))
                    self.h[i] = h       
                hs.append(h)
            return hs
        else:
            raise NotImplementedError("Argument of indexing must be either an int or a slice object")

