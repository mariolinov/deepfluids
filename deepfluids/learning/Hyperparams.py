import json


class Hyperparams(object):
    def __init__(self,
                 learning_rate=None,
                 batch_size=None,
                 batch_first=None,
                 n_frames_in=None,
                 n_frames_out=None,
                 n_frames_skip=None,
                 time_step=None):

        self.learning_rate = learning_rate
        self.batch_size = batch_size or 1
        self.batch_first = batch_first or True
        self.n_frames_in = n_frames_in or 5
        self.n_frames_out = n_frames_out or 5
        self.n_frames_skip = n_frames_skip or 0
        self.time_step = time_step or 1

        return

    def __repr__(self):
        return repr(self.__dict__)

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    @staticmethod
    def from_dict(dic):
        return Hyperparams(learning_rate=dic.get("learning_rate", None),
                           batch_size=dic.get("batch_size", None),
                           batch_first=dic.get("batch_first", None),
                           n_frames_in=dic.get("n_frames_in", None),
                           n_frames_out=dic.get("n_frames_out", None),
                           n_frames_skip=dic.get("n_frames_skip", None),
                           time_step=dic.get("time_step", None))

    @staticmethod
    def from_json(filepath):
        with open(filepath, "r") as file:
            content = json.load(file)
        return Hyperparams.from_dict(content)
