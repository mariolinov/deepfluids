import json


class ConvNetHyperparams(object):
    def __init__(self,
                 kernel_size=None,
                 stride=None,
                 padding=None,
                 pooling=None,
                 depth=None,
                 filters_dimensions=None,
                 input_size=None,
                 output_size=None,
                 batch_norm=None,
                 attention=None):

        if ((filters_dimensions is None or len(filters_dimensions) <= 0) and depth is not None):
            self.depth = depth
        elif (depth is None or depth <= 0 and filters_dimensions is not None):
            self.filters_dimensions = filters_dimensions
        else:
            raise ValueError("You need to set either the depth of the network or specify the dimensions of the filters")

        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.pooling = pooling
        self.input_size = input_size
        self.output_size = output_size
        self.batch_norm = batch_norm
        self.attention = attention or 0
        return

    def __repr__(self):
        return repr(self.properties_dict())

    def __str__(self):
        return str(self.properties_dict())

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    @property
    def depth(self):
        return self._depth

    @depth.setter
    def depth(self, value):
        self._depth = value
        self._filters_dimensions = [2 ** (self._depth + i) for i in range(self._depth)]
        return

    @property
    def filters_dimensions(self):
        return self._filters_dimensions

    @filters_dimensions.setter
    def filters_dimensions(self, value):
        self._filters_dimensions = value
        self._depth = len(self._filters_dimensions)
        return

    @property
    def kernel_size(self):
        return self._kernel_size

    @kernel_size.setter
    def kernel_size(self, value):
        self._kernel_size = self.pair_args(value)

    @property
    def stride(self):
        return self._stride

    @stride.setter
    def stride(self, value):
        self._stride = self.pair_args(value)

    @property
    def padding(self):
        return self._padding

    @padding.setter
    def padding(self, value):
        self._padding = self.pair_args(value)

    @property
    def pooling(self):
        return self._pooling

    @pooling.setter
    def pooling(self, value):
        self._pooling = self.pair_args(value)

    def pair_args(self, args):
        if type(args) != list:
            args = [args] * self.depth
        elif len(args) != self.depth:
            args = args + [args[-1]] * (self.depth - len(args))  # repeating the last
        else:
            raise ValueError("The number of specified kernels is different from the encoder depth")

        assert len(args) == self.depth
        return args

    def properties_dict(self):
        dic = {
            "filters_dimensions": self.filters_dimensions,
            "kernel_size": self.kernel_size,
            "stride": self.stride,
            "padding": self.padding,
            "pooling": self.pooling,
            "input_size": self.input_size,
            "output_size": self.output_size,
            "depth": self.depth,
            "batch_norm": self.batch_norm,
            "attention": self.attention
        }
        return dic

    @staticmethod
    def from_dict(dic):
        return ConvNetHyperparams(kernel_size=dic.get("kernel_size", None),
                                  stride=dic.get("stride", None),
                                  padding=dic.get("padding", None),
                                  pooling=dic.get("pooling", None),
                                  input_size=dic.get("input_size", None),
                                  depth=dic.get("depth", None),
                                  batch_norm=dic.get("batch_norm", None),
                                  attention=dic.get("attention", None),
                                  filters_dimensions=dic.get("filters_dimensions", None))

    @staticmethod
    def from_json(filepath):
        with open(filepath, "r") as file:
            content = json.load(file)
        return ConvNetHyperparams.from_dict(content)
