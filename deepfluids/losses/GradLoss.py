import torch.nn as nn
#from .MSLELoss import MSLELoss 
from ..utils.calculus import grad2D

class GradLoss(nn.Module):

    def __init__(self, g=0.05, L=(1,1)):
        super().__init__()

        if g > 1 or g < 0:
            raise ValueError("g must between 0 and 1.")
        if len(L) != 2:
            raise ValueError("L must be a 2-dimensional tuple.")
        self.g = g
        self.L = L

    def forward(self,output,target):
    
        if not (target.shape == output.shape):
            raise ValueError("Target size ({}) must be the same as input size ({})".format(target.shape, output.shape))

        # Inputs dimensions [Batch,W,H]
        nx, ny = output.shape[1:]
        dx, dy = self.L[0]/(nx-1), self.L[1]/(ny-1)
        
        # Compute the gradients of the output and the target
        o_grax_x, o_grax_y = grad2D(output[:,:,:],dx,dy)
        t_grax_x, t_grax_y = grad2D(target[:,:,:],dx,dy)

        # Create dimension for channel
        output = output.unsqueeze(1)
        target = target.unsqueeze(1)

        mse  = nn.MSELoss()
        #msle = MSLELoss()

        return (1-self.g)*mse(output,target) + self.g*( mse(o_grax_x,t_grax_x) + mse(o_grax_y,t_grax_y) )
