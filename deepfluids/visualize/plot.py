import numpy as np
import matplotlib.pyplot as plt


def plot_field(prediction, truth, omega, vmin, vmax,cmap="coolwarm"):
    
    assert len(prediction) == len(truth), "prediction and truth must have the same length to be compared"
    
    _, plots = plt.subplots(2, len(prediction), figsize=(32, 32 * 2.5 / len(prediction)))
    omega = omega.detach().cpu().squeeze()
    solid = np.where(omega>0.5, True, False)

    for i, frame in enumerate(prediction):
        frame = frame.detach().cpu().squeeze()
        frame[solid] = np.nan
        plots[0, i].imshow(frame, vmin=vmin, vmax=vmax,cmap=cmap)

    for i, frame in enumerate(truth):
        frame = frame.detach().cpu().squeeze()
        frame[solid] = np.nan
        plots[1, i].imshow(frame, vmin=vmin, vmax=vmax,cmap=cmap)

    return
