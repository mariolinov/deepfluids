import torch
from torch import nn


class UNetConvBlock(nn.Sequential):
    def __init__(self, in_channels, out_channels, padding, batch_norm):
        super(UNetConvBlock, self).__init__()

        # add convolution 1
        self.add_module("conv1",
                        nn.Conv2d(in_channels=in_channels,
                                  out_channels=out_channels,
                                  kernel_size=3,
                                  padding=int(padding)))
        self.add_module("relu1", nn.ReLU())

        # add batchnorm 1
        if batch_norm:
            self.add_module("batchnorm1", nn.BatchNorm2d(out_channels))

        # add convolution 2
        self.add_module("conv2",
                        nn.Conv2d(in_channels=out_channels,
                                  out_channels=out_channels,
                                  kernel_size=3,
                                  padding=int(padding)))
        self.add_module("relu2", nn.ReLU())

        # add batchnorm 2
        if batch_norm:
            self.add_module("batchnorm2", nn.BatchNorm2d(out_channels))

    def forward(self, x):
        return super().forward(x)


class UNetUpBlock(nn.Module):
    def __init__(self, in_channels, out_channels, padding, batch_norm, bilinear):
        super(UNetUpBlock, self).__init__()

        # upsample
        if bilinear:
            self.up = nn.Sequential(nn.Upsample(mode='bilinear', scale_factor=2),
                                    nn.Conv2d(in_channels, out_channels, kernel_size=1))
        else:
            self.up = nn.ConvTranspose2d(in_channels  = in_channels,
                                         out_channels = out_channels,
                                         kernel_size  = 2,
                                         stride       = 2)

        # add convolutions block
        self.conv_block = UNetConvBlock(in_channels=in_channels,
                                        out_channels=out_channels,
                                        padding=padding,
                                        batch_norm=batch_norm)
    # @staticmethod
    # def center_crop(layer, target_size):
    #     _, _, layer_height, layer_width = layer.size()
    #     diff_y = (layer_height - target_size[0]) // 2
    #     diff_x = (layer_width - target_size[1]) // 2
    #     return layer[:, :, diff_y:(diff_y + target_size[0]), diff_x:(diff_x + target_size[1])]

    def forward(self, x, skip_connection):
        up = self.up(x)
        #crop = self.center_crop(skip_connection, up.shape[2:])
        out = torch.cat([up, skip_connection], 1)
        out = self.conv_block(out)

        return out
