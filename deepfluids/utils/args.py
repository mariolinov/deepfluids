import numpy as np


def expand_args(x, base, pr_name='parameter to set'):
    # not sure I covered all cases
    if type(x) == type(base):
        if type(x) == list:
            if len(x) == len(base):
                return x
            elif len(x) == 1:
                return [x[0]] * len(base)
            else:
                raise ValueError('Dimension of requested {} is ambiguous and not coherent with other parameters')
        elif type(x) == np.ndarray:
            if x.size == base.size:
                return x
            elif x.size == 1:
                return x + np.zeros_like(base)
            else:
                raise ValueError('Dimension of requested {} is ambiguous and not coherent with other parameters')
    elif type(x) == int or type(x) == float:
        if type(base) == list:
            return [x]*len(base)
        elif type(base) == np.ndarray:
            return x + np.zeros_like(base)
    elif type(x) == np.ndarray:
        if x.size == len(base):
            return x
        elif x.size == 1:
            return [x[0]]*len(base)
