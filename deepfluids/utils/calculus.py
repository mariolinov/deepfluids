import torch

def grad2D(u,dx,dy):
    
    batch_size, nx, ny = u.shape # Get diemnsions of the structured mesh
    device = u.device
    cx = 1/(2*dx)    # Finite differences coefficient in x direcition 
    cy = 1/(2*dy)    # Finite differences coefficient in y direcition

    grad_x = torch.zeros(batch_size,nx,ny).to(device)   # Initilise grad in x direcition
    grad_y = torch.zeros(batch_size,nx,ny).to(device)   # Initilise grad in y direcition 

    grad_x[:,0,:] = (-3*u[:,0,:]+4*u[:,1,:]-u[:,2,:])/(2*dx)
    for i in range(1,nx-1):
        grad_x[:,i,:] = cx*( u[:,i+1,:] - u[:,i-1,:] )
    grad_x[:,-1,:] = (u[:,-3,:]-4*u[:,-2,:]+3*u[:,-1,:])/(2*dx)

    
    grad_y[:,:,0] = (-3*u[:,:,0]+4*u[:,:,1]-u[:,:,2])/(2*dy)
    for j in range(1,ny-1):
        grad_y[:,:,j] = cx*( u[:,:,j+1] - u[:,:,j-1] )
    grad_y[:,:,-1] = (u[:,:,-3]-4*u[:,:,-2]+3*u[:,:,-1])/(2*dy)

    return grad_x, grad_y