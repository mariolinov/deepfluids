import torch


class GaussianNoise(object):
    """
    Adds gaussian noise to the input_data and target_data data

    Args:
        c: constant to multiply
    """

    def __init__(self, snr, normalise_below=200):
        # snr is the SNR
        self.snr = snr
        self.normalise_below = normalise_below

    def __call__(self, sample):
        sample['Input'] = [self.gaussian_noise(inp, self.snr) for inp in sample['Input']]
        sample['Output'] = [self.gaussian_noise(inp, self.snr) for inp in sample['Output']]
        return sample

    @staticmethod
    def gaussian_noise(tensor, snr, normalise_below=200):
        # Do not normalise if the SNR is above the threshold: image is noisy enough already
        if snr > normalise_below:
            return tensor

        # generate standard noise
        noise = torch.randn(*(tensor.shape))

        # compute the power of the signal and the noise
        power_signal = torch.mean(tensor ** 2)
        power_noise = torch.mean(noise**2)

        # compute multiplier for the noise to achieve desired SNR
        factor = power_signal / power_noise * (10.0 ** (-snr / 10.))
        noise = noise * torch.sqrt(factor)
        return (tensor + noise)
