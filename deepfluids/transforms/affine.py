import numpy as np


class AffineN2(object):  # !! I have no idea what the aim of this functions is
    """Add and multiply by random constants the min and max of sample,
       as well as the target. This is for when I need to modify data normalised with Normal2

    Args:
        a: constant to multiply
        b: constant to add
    This one operates on numpy arrays
    """

    def __init__(self, a=None, b=None, seed=None):
        # should probably use a different distribution
        self.a = a or np.random.randn(seed)
        self.b = b or np.random.randn(seed)

    def __call__(self, sample):
        sample['mint'] = (sample['mint'] * self.a + self.b)
        sample['maxt'] = (sample['maxt'] * self.a + self.b)
        sample['Target'] = (sample['Target'] * self.a + self.b)
        return sample


class AffineNT2(object):  # !! I have no idea what the aim of this functions is
    """Add and multiply by random constants the min and max of sample,
       as well as the target. This is for when I need to modify data normalised with Normal2

    Args:
        a: constant to multiply
        b: constant to add
    """
    def __init__(self, a=None, b=None, seed=None):
        self.a = a or np.random.randn(seed).astype('float32')
        self.b = b or np.random.randn(seed).astype('float32')

    def __call__(self, sample):
        sample['mint'] = (sample['mint'] * self.a + self.b)
        sample['maxt'] = (sample['maxt'] * self.a + self.b)
        sample['Target'].mul_(self.a).add_(self.b)
        return sample


class Affine(object):  # !! I have no idea what the aim of this functions is
    """Add and multiply by random constants to all the images in a sample,
       both Input and target
    Args:
        a: multiplicative constant
        b: additive constant
    """
    def __init__(self, a=None, b=None, seed=None):
        self.a = a or np.random.randn(seed).astype('float32')
        self.b = b or np.random.randn(seed).astype('float32')

    def __call__(self, sample):
        input_data, target_data = sample['Input'], sample['Target']

        sample['Input'] = (self.a * input_data + self.b).astype('float32')
        sample['Target'] = (self.a * target_data + self.b).astype('float32')
        return sample
