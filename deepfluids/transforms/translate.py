import torch
import numpy as np
import random
import statistics


class Translate(object):
    """
    h5 files
    """

    def __init__(self, shift, axis=0):
        self.shift = shift
        self.axis = axis

    def __call__(self, sample):

        mean = statistics.mean( [np.mean(arr).item() for arr in sample["Input"]][1:] )
        sample['Input'][1:]  = [self.translate(arr, self.shift, mean, axis=self.axis) for arr in sample['Input'][1:] ]
        sample['Output'] = [self.translate(arr, self.shift, mean, axis=self.axis) for arr in sample['Output']]

        if self.axis == 0:
            if self.shift > 0:
                sample['Input'][0][:self.shift,:] = 1
            elif self.shift < 0:
                sample['Input'][0][self.shift:,:] = 1

        if self.axis == 1:
            if self.shift > 0:
                sample['Input'][0][:,:self.shift] = 1
            elif self.shift < 0:
                sample['Input'][0][:,self.shift:] = 1
        

        return sample

    @staticmethod
    def translate(arr, shift, mean, axis=0):
        arr = np.roll(arr, shift, axis=axis)

        if axis == 0:
            if shift > 0:
                arr[:shift,:] = mean
            elif shift < 0:
                arr[shift:,:] = mean

        if axis == 1:
            if shift > 0:
                arr[:,:shift] = mean
            elif shift < 0:
                arr[:,shift:] = mean
        
        return arr