import torchvision.transforms.functional as F
import numpy as np
from PIL import Image
from scipy.interpolate import interp2d


class Resize(object):
    """
    Convert ndarrays in sample to Tensors.
    """
    def __init__(self, size):
        self.size = size

    def __call__(self, sample):
        # do not swap axis
        sample["Input"]  = [resize_2darray(image, self.size) for image in sample["Input"]]
        sample["Output"] = [resize_2darray(image, self.size) for image in sample["Output"]]
        sample["Omega"]  = resize_2darray(sample["Omega"], self.size)

        return sample

def resize_2darray(array,size):
    x  = np.linspace(0, 1, array.shape[1])
    y  = np.linspace(0, 1, array.shape[0])
    f  = interp2d(x, y, array, kind='cubic')
    xi = np.linspace(0, 1, size[0])
    yi = np.linspace(0, 1, size[1])
    array = f(xi,yi)
    return array
