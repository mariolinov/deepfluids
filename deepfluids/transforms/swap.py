import random


class SwapInputOutput(object):
    """
    Swaps input and output of a sequence. If it is a temporal sequence this means predicting the past from the future.
    """

    def __init__(self, probability=0.5):
        super().__init__()
        self.probability = probability
        return

    def __call__(self, sample):
        if random.rand() > self.probability:
            sample_input = sample['Input']
            sample_output = sample['Output']

            # guessing 3-episodes-length subsequences, sequence is {Input: (A, B, C), Output: (D, E, F)}
            sample['Input'] = list(reversed(sample_output))  # (A, B, C) -> (F, E, D)
            sample['Output'] = list(reversed(sample_input))  # (D, E, F) -> (C, B, A)
        return sample  # {Input: (F, E, D), Output: (C, B, A)}
