import torchvision.transforms.functional as F
from PIL import Image
import numpy as np
import random


class RandomRotate(object):
    """
    Rotates a numpy array by 90 degrees on the place defined by the 2nd and 3rd axes, similar to rotating an image
    """
    def __init__(self, degrees, probability=0.5):
        self.degrees = degrees
        self.probability = probability

    def __call__(self, sample):
        if random.random() < self.probability:
            if type(sample["Input"][0]) == np.ndarray:
                if self.degrees != 90 and self.degrees != 180 and self.degrees != 270:
                    raise Exception("90, 180 and 270 deg rotations are only possible.")
                sample['Input']  = [np.rot90(img, k=self.degrees/90) for img in sample['Input'] ]
                sample['Output'] = [np.rot90(img, k=self.degrees/90) for img in sample['Output']]
                if sample["Omega"].any():
                    sample["Omega"] = np.rot90(sample['Omega'], k=self.degrees/90)

            elif type(sample["Input"][0]) == Image.Image:
                sample['Input']  = [F.rotate(img, self.degrees) for img in sample['Input'] ]
                sample['Output'] = [F.rotate(img, self.degrees) for img in sample['Output']]
        
        return sample
