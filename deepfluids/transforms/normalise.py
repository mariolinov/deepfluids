import numpy as np
import torch
import statistics


class Normalise(object):
    """Normalize an tensor image with mean and standard deviation.
    Given mean: ``(M1,...,Mn)`` and std: ``(S1,..,Sn)`` for ``n`` channels, this transform
    will normalize each channel of the input ``torch.*Tensor`` i.e.
    ``input[channel] = (input[channel] - mean[channel]) / std[channel]``

    Args:
        mean (sequence): Sequence of means for each channel.
        std (sequence): Sequence of standard deviations for each channel.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, sample):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.

        Returns:
            Tensor: Normalized Tensor.
        """
        return {"Input": [self.normalise(tensor, self.mean, self.std) for tensor in sample["Input"]],
                "Output": [self.normalise(tensor, self.mean, self.std) for tensor in sample["Output"]]}

    @staticmethod
    def normalise(tensor, mean, std):
        """
        Normalize a tensor image with mean and standard deviation.
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
            mean (sequence): Sequence of means for each channel.
            std (sequence): Sequence of standard deviations for each channely.

        Returns:
            Tensor: Normalized Tensor image.
        """
        # TODO: optimise
        # EP. TODO: Are we sure that modifying the object in place doesn't creates troubles in the data flow?
        # What if I recalculate the statistics at one point? They will be different.
        tensor.sub_(mean).div_(std)
        return True



class UnitaryScale(object):

    def __init__(self,omega=False):
        self.omega = omega

    def __call__(self, sample):

        if self.omega:
            ini = 1
        else:
            ini = 0    

        vmin = min([torch.min(tensor).item() for tensor in sample["Input"][ini:]    ])
        vmax = max([torch.max(tensor).item() for tensor in sample["Input"][ini:]    ])

        # in-place normalisation
        sample['Input'][ini:]  = [self.unitaryScale(tensor, vmin, vmax) for tensor in sample["Input"][ini:]]
        sample["Output"]       = [self.unitaryScale(tensor, vmin, vmax) for tensor in sample["Output"]     ]
        
        return sample

    @staticmethod
    def unitaryScale(tensor, vmin, vmax):
        return tensor.sub_(vmin).div_(vmax-vmin)



class ZeroMeanScale(object):

    def __call__(self, sample):
        solid = torch.tensor(sample["Omega"], dtype=torch.bool)
        fluid = solid.logical_not()
        
        mean = statistics.mean( [torch.mean(tensor[fluid]).item() for tensor in sample["Input"]] )

        # in-place normalisation
        sample['Input']  = [(tensor-mean)*10 for tensor in sample["Input"] ]
        sample["Output"] = [(tensor-mean)*10 for tensor in sample["Output"]]
        return sample

class Scale(object):

    def __init__(self, x1, x2):
        self.x1 = x1
        self.x2 = x2

    def __call__(self, sample):

        sample['Input']  = [(tensor-self.x1)/(self.x2-self.x1) for tensor in sample["Input"] ]
        sample["Output"] = [(tensor-self.x1)/(self.x2-self.x1) for tensor in sample["Output"]]
        return sample

    def back(self, x):
        for i, frame in enumerate(x):
            x[i] = self.x1 + (self.x2 - self.x1)*frame
        return x



class ScaleTanh(object):

    def __init__(self, x1, x2):
        self.x1 = x1
        self.x2 = x2
        self.shape_factor = 2

    def __call__(self, sample):

        sample['Input']  = [torch.tanh(self.shape_factor*(tensor-self.x1)/(self.x2-self.x1))/np.tanh(self.shape_factor) for tensor in sample["Input"] ]
        sample["Output"] = [torch.tanh(self.shape_factor*(tensor-self.x1)/(self.x2-self.x1))/np.tanh(self.shape_factor) for tensor in sample["Output"]]
        return sample


class ZeroMeanTanh(object):

    def __call__(self, sample):

        solid = torch.tensor(sample["Omega"], dtype=torch.bool)
        fluid = solid.logical_not()
        
        mean    = statistics.mean( [torch.mean(tensor[fluid]).item() for tensor in sample["Input"]])
        max_abs = max([torch.max(torch.abs(tensor[fluid])).item() for tensor in sample["Input"]])

        # in-place normalisation
        sample['Input']  = [torch.tanh(tensor/mean-1)/np.tanh(max_abs/mean-1) for tensor in sample["Input"] ]
        sample["Output"] = [torch.tanh(tensor/mean-1)/np.tanh(max_abs/mean-1) for tensor in sample["Output"]]
        return sample


class Zscore(object):

    def __call__(self, sample):

        solid = torch.tensor(sample["Input"][0], dtype=torch.bool)
        fluid = solid.logical_not()
        
        mean = statistics.mean(  [torch.mean(tensor[fluid]).item() for tensor in sample["Input"][1:]] )
        std  = statistics.stdev( [torch.std( tensor[fluid]).item() for tensor in sample["Input"][1:]] )

        # in-place normalisation
        sample['Input'][1:] = [(tensor-mean)/std for tensor in sample["Input"][1:]]
        sample["Output"]    = [(tensor-mean)/std for tensor in sample["Output"]   ]
        return sample


class Round(object):
    """
    Round a tensor to the given decimal position
    """

    def __init__(self,decimal_pos):
        self.decimal_pos = decimal_pos

    def __call__(self, sample):
        sample['Input']  = [self.roundTensor(tensor, self.decimal_pos ) for tensor in sample["Input"]]
        sample["Output"] = [self.roundTensor(tensor, self.decimal_pos ) for tensor in sample["Output"]]
        return sample

    @staticmethod
    def roundTensor(tensor, decimal_pos):
        return torch.round(tensor * 10**decimal_pos) / (10**decimal_pos)

class LikeImg(object):
    """
    Scale h5 files to the jpg format
    """

    def __call__(self, sample):
        sample['Input']  = [self.likeImg(tensor) for tensor in sample["Input"]]
        sample["Output"] = [self.likeImg(tensor) for tensor in sample["Output"]]
        return sample

    @staticmethod
    def likeImg(tensor):
        return (tensor - 0.97)/(1.11-0.97)*255


class MapToCodomain(object):
    def __init__(self, domain=None, codomain=None):
        """
        Args:
            domain: range to map from as a two-item tuple
            codomain: range to to as a two-item tuple
        """
        self.domain = domain
        self.codomain = codomain

    def __call__(self, array):
        return self.to_codomain(array, self.domain, self.codomain)

    @staticmethod
    def to_codomain(array, domain=None, codomain=None):
        """
        Args:
            domain: range to map from as a two-item tuple
            codomain: range to to as a two-item tuple
        """
        if type(array) == torch.Tensor:
            array = array.numpy()  # Should I cpu().detach() here, maybe?

        if domain is None or domain[0] is None or domain[1] is None:
            domain = (np.min(array), np.max(array))

        if codomain is None:
            codomain = (-1, 1)

        return (((codomain[1] - codomain[0]) * array - domain[1] - domain[0]) / (domain[1] - domain[0])) + codomain[0]


class Centre(object):
    """
    A utility class that performs translation on a set of data
    """
    def __init__(self, stats):
        """
        Args:
            stats (Dict): a dictionary that stores the info about the range of the dataset.
        """
        self.stats = stats

    def __call__(self, sample):
        """
        Args:
            sample (Dict): a dictionary that contains the information of Input and Target data

        Returns:
            Dict: The input dictionary with the values translated by the median of the range
        """
        # Key check here is missing as I can just raise an error if that's not found
        # centering the input
        input_stats = self.stats["Input"]
        self.translate_by_median(sample["Input"], input_stats["min"], input_stats["max"])

        # centering the targets
        target_stats = self.stats["Target"]
        self.translate_by_median(sample["Target"], target_stats["min"], target_stats["max"])
        return sample

    @staticmethod
    def translate_by_median(tensor, minvalue, maxvalue):
        # type: (torch.Tensor, float) -> None
        tensor.sub_(maxvalue - minvalue)

    @staticmethod
    def centre(tensor, mean):
        # type: (torch.Tensor, float) -> None
        tensor.sub_(mean)



    @staticmethod
    def scale(tensor, std):
        # type: (torch.Tensor, float) -> None
        tensor.div_(std)
