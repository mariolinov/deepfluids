import torch
import numpy as np
import random
from scipy.ndimage import rotate

class Crop(object):

    def __init__(self, rot=0, shift_x=(0,0), shift_y=(0,0)):
        self.rotate = rot
        self.shift_x = shift_x # in [0,1]
        self.shift_y = shift_y # in [0,1]

    def __call__(self, sample):
        rot = self.rotate*random.randint(0,8)
        shift_x = self.shift_x[0] + random.random()*(self.shift_x[1] - self.shift_x[0])
        shift_y = self.shift_y[0] + random.random()*(self.shift_y[1] - self.shift_y[0] )

        sample['Omega']  = self.crop(sample['Omega'], rot, shift_x, shift_y)
        sample['Input']  = [self.crop(arr, rot, shift_x, shift_y) for arr in sample['Input'] ]
        sample['Output'] = [self.crop(arr, rot, shift_x, shift_y) for arr in sample['Output']]
        return sample

    @staticmethod
    def crop(arr, rot, shift_x, shift_y):
        # First rotate
        if rot: arr = rotate(arr, rot, reshape=False, order=5, cval=np.nan)
        # Translate
        width, height = arr.shape[0]//4, arr.shape[1]//4
        shift_x = int(width*shift_x)
        shift_y = int(height*shift_y)
        arr = np.roll(arr,shift_x,axis=0)
        arr = np.roll(arr,shift_y,axis=1)
        # Crop
        return arr[width:-width,height:-height]