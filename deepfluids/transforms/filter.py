import numpy as np
import statistics

class Solid2Mean(object):

    def __call__(self, sample):

        solid = np.array(sample["Omega"], dtype=np.bool)
        fluid = np.logical_not(solid)
        
        mean    = statistics.mean( [np.mean(arr[fluid]) for arr in sample["Input"]])

        sample['Input']  = [self.replace(arr, solid, mean) for arr in sample["Input"] ]
        sample["Output"] = [self.replace(arr, solid, mean) for arr in sample["Output"]]

        return sample

    @staticmethod
    def replace(arr, solid, new_value):
        arr[solid] = new_value
        return arr


class Solid2Value(object):

    def __init__(self, s):

        self.s = s

    def __call__(self, sample):

        solid = np.isnan(sample["Input"][0])

        sample['Input']  = [self.replace(arr, solid, self.s) for arr in sample["Input"] ]
        sample["Output"] = [self.replace(arr, solid, self.s) for arr in sample["Output"]]

        return sample

    @staticmethod
    def replace(arr, solid, new_value):
        arr[solid] = new_value
        return arr