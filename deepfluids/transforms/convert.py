import torch
import numpy as np
from PIL import Image
import torchvision.transforms.functional as F


class ToTensor(object):
    """
    Convert ndarrays in sample to Tensors.
    """

    def __call__(self, sample):
        # do not swap axis
        if type(sample["Input"][0]) == np.ndarray:
            sample['Input']  = [torch.tensor(image.copy(),dtype=torch.float).unsqueeze(0) for image in sample['Input']]
            sample['Output'] = [torch.tensor(image.copy(),dtype=torch.float).unsqueeze(0) for image in sample['Output']]
            if sample["Omega"].any():
                sample["Omega"] = torch.tensor(sample["Omega"].copy(),dtype=torch.float).unsqueeze(0)

        elif type(sample["Input"][0]) == Image.Image:
            sample['Input']  = [F.to_tensor(image) for image in sample['Input']]
            sample['Output'] = [F.to_tensor(image) for image in sample['Output']]

        return sample
