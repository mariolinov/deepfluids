import numpy as np
import random
import torchvision.transforms.functional as F
import numpy as np
from PIL import Image


class RandomHFlip(object):
    '''
    Randomly flips a np.ndarray of size (C, D, H, W) on the H dimension,
    that is aroud x = 0. The flip happens with probability p.
    default for p is 0.5
    '''
    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        if random.random() < self.p:
            if type(sample["Input"][0]) == np.ndarray:
                sample['Input']  = [np.flip(img,axis=0) for img in sample['Input'] ]
                sample['Output'] = [np.flip(img,axis=0) for img in sample['Output']]
                if sample["Omega"].any():
                    sample["Omega"] = np.flip(sample['Omega'], axis=0)

            elif type(sample["Input"][0]) == Image.Image:
                sample['Input']  = [F.hflip(inp) for inp in sample['Input']]
                sample['Output'] = [F.hflip(inp) for inp in sample['Output']]
        return sample


class RandomVFlip(object):
    '''
    Randomly flips a np.ndarray of size (C, D, H, W) on the W dimension,
    that is aroud x = 0. The flip happens with probability p.
    default for p is 0.5
    '''
    def __init__(self, p=0.5):
        self.p = p

    def __call__(self, sample):
        if random.random() < self.p:
            if type(sample["Input"][0]) == np.ndarray:
                sample['Input']  = [np.flip(img,axis=1) for img in sample['Input'] ]
                sample['Output'] = [np.flip(img,axis=1) for img in sample['Output']]
                if sample["Omega"].any():
                    sample["Omega"]  = np.flip(sample['Omega'], axis=1)

            elif type(sample["Input"][0]) == Image.Image:
                sample['Input']  = [F.vflip(inp) for inp in sample['Input']]
                sample['Output'] = [F.vflip(inp) for inp in sample['Output']]

        return sample
