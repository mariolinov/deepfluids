import os
import torch
import time
import random
from torch import nn
from torchvision import transforms
import torch.nn.functional as F
from ..modules.UNetModules import UNetConvBlock, UNetUpBlock
from ..utils.log import log
from tqdm import tqdm
from torch.utils.tensorboard import SummaryWriter


class UNet(nn.Module):
    def __init__(self,
                 hyperparams,
                 unet_hyperparams,
                 omega=False,
                 bilinear=False):
        super().__init__()
        self.unet_hyperparams = unet_hyperparams
        self.hyperparams = hyperparams
        self.omega = omega

        # downsampling
        self.downsample = nn.ModuleList()
        if self.omega:
            in_channels = hyperparams.n_frames_in + 1
        else:
            in_channels = hyperparams.n_frames_in
                
        for i in range(unet_hyperparams.depth):
            self.downsample.append(UNetConvBlock(in_channels=in_channels,
                                                 out_channels=unet_hyperparams.filters_dimensions[i],
                                                 padding=unet_hyperparams.padding[i],
                                                 batch_norm=unet_hyperparams.batch_norm))
            in_channels = unet_hyperparams.filters_dimensions[i]

        # upsampling
        self.upsampling = nn.ModuleList()
        for i in reversed(range(unet_hyperparams.depth - 1)):
            self.upsampling.append(UNetUpBlock(in_channels=in_channels,
                                               out_channels=unet_hyperparams.filters_dimensions[i],
                                               padding=unet_hyperparams.padding[i],
                                               batch_norm=unet_hyperparams.batch_norm,
                                               bilinear=bilinear))

            in_channels = unet_hyperparams.filters_dimensions[i]

        # return only one frame out
        self.output = nn.Conv2d(in_channels=in_channels, out_channels=1, kernel_size=1)


    def forward(self, x):
        skip_connections = []
        for i, down in enumerate(self.downsample):
            x = down(x)
            if i != len(self.downsample) - 1:
                skip_connections.append(x)
                x = F.max_pool2d(x, 2)


        for i, up in enumerate(self.upsampling):
            x = up(x, skip_connections[-i - 1])

        #x = F.interpolate(x,scale_factor=1.1,mode="bilinear",align_corners=True)
        #x = self.center_crop(x, self.unet_hyperparams.input_size)
        x = self.output(x)

        return x


    def fit(self, hyperparams, loss_fn, optimiser, train_loader, test_loader=None, scheduler=None, epochs=1, save_info=None, board=None):

        device = next(self.parameters()).device
        if board: writer = SummaryWriter(board)

        for epoch in tqdm(range(1,epochs+1), desc="Completed epochs", leave=False, position=0):
            
            self.train()
            training_loss = 0.
            n_batches = len(train_loader)

            for j, sample in enumerate(train_loader):  # for each batch
                j += 1
                training_loss += self.fit_batch(epoch, hyperparams, sample, loss_fn, optimiser, device=device).item()
                log(total_loss=training_loss, epoch=epoch, batch_number=j, n_batches=n_batches)

            if board:
                writer.add_scalar('Loss/train', training_loss, epoch)

            if test_loader:    
                self.eval()
                validation_loss = 0
                for iter, sample in enumerate(test_loader):
                    loss = self.test_batch(hyperparams, sample, device=device)
                    validation_loss += loss
                validation_loss /= iter+1
                if board:
                    writer.add_scalar('Loss/validation', validation_loss, epoch)

            if scheduler:
                    scheduler.step(training_loss)

            if save_info and not epoch%save_info["interval"]:
                path = os.path.join(save_info["root"], save_info["name"]+".pt")
                torch.save(self.state_dict(), path)

        writer.close()

        return self


    def fit_batch(self, epoch, hyperparams, batch, loss_fn, optimiser, device=None):
        """
        Performs a parameters update by processing a single batch
        Args:
            hyperparams (deepheart.learning.Hyperparams): Contains information about the learning routing, such as the batch_size
            batch (Dict[str: List[torch.Tensor]]): an unstacked number of samples. Contains two keys "Input" and "Output".
                                                   The values are a list of torch.Tensor of length batch_size
            loss_fn (torch.nn.Module): the loss criterion to optimise
            optimiser (torch.optim.Optimizer): The algorithm used to perform the optimisation
            device (torch.device): You can use `device = torch.device("cuda" if torch.cuda.is_available() else "cpu")` to be hardware agnostic
        Returns:
            A tuple with the model itself and the losses at each iteration (for each batch, for each epoch)
        """
        if self.omega:
            X = torch.stack([batch["Omega"],*batch["Input"]], dim=1).squeeze().to(device)
        else:
            X = torch.stack(batch["Input"] , dim=1).squeeze(2).to(device)
        Y = torch.stack(batch["Output"], dim=1).squeeze(2).to(device)


        for i in range(hyperparams.n_frames_out):

            # inject prediction into the input
            if i != 0:
                X = self.shift_and_replace(X, y_hat)  # (B, n_frames_out, W, H)

            y_hat = self(X)  # (B, 1, W, H)
            loss = loss_fn(y_hat.squeeze(), Y[:, i])  # Y[:, i] takes only the ith frame, which is the one we are predicting at the ith iteration
            loss.backward(retain_graph=True)

        if epoch > 50 and loss > 0.5:
            return 0

        optimiser.step()
        optimiser.zero_grad()

        return loss

    def test_batch(self, hyperparams, batch, device=None):
        
        if self.omega:
            X = torch.stack([batch["Omega"],*batch["Input"]], dim=1).squeeze(2).to(device)
        else:
            X = torch.stack(batch["Input"] , dim=1).squeeze(2).to(device)
        Y = torch.stack(batch["Output"], dim=1).squeeze(2).to(device)

        pred = self.infer(X, hyperparams.n_frames_out)
        pred = torch.stack(pred, dim=1).squeeze(2).to(device)

        loss_fn = nn.MSELoss()
        loss = loss_fn(pred, Y).item()

        return loss


    def infer(self, X, n_frames_out):

        self.eval()
        with torch.no_grad():
            out_frames = []
            for i in range(n_frames_out):
                y_hat = self.forward(X)
                X = self.shift_and_replace(X, y_hat)
                out_frames.append(y_hat)
        return out_frames

    def shift_and_replace(self, X, new_slice):
        """
        Given a sequence of frames, shifts the frames in time and replaces the last one with a desider value.
        Returns:
            Given a series of frames (A, B, C, D, E), and a new frame Y, the method performs the two following operations:
            Shift: (A, B, C, D, E) -> (B, C, D, E, A)
            Replace: (B, C, D, E, A) -> (B, C, D, E, Y)
        """
        X = torch.roll(X, -1, dims=1)
        if self.omega:
            X[:,0,:,:] = X[:,-1,:,:]
        X[:, -1:, :, :] = new_slice  # (B, n_frames_out, W, H)

        return X