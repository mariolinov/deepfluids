from setuptools import setup, find_packages

setup(
name='deepfluids',
description='Solve flow motion using deep learning techniques',
author='DeepX',
author_email='',
license='GPL',
packages=find_packages(),
version="0.0.0"
)