## Code for the paper "Simulating Surface Wave Dynamics with Convolutional Networks" ([https://inductive-biases.github.io/papers/31.pdf](https://inductive-biases.github.io/papers/31.pdf))

### Installation
Create a conda envioroment named df: &nbsp; ```conda env create -f env/df.yml```
After cloning the repository, install it with: &nbsp; ```cd deepfluids; pip install -e .```

To test that deepfluids is working run the file ```examples/training.py```.
To train the network you will need access to the datasets available at [x](x).
Once the training begins, you will see a progress bar indicating the completed epochs. You can also check the by running ```--logdir="<name of the log file>"```.
An example of testing a trained model is in file ```examples/testing.ipynb```.

### To cite deepfluids, use the following reference:

Mario Lino, Chris Cantwell, Stathi Fotiadis, Eduardo Pignatelli, and Anil A. Bharath.
Simulating surface wave dynamics with convolutional networks. In NeurIPS Workshop
on Interpretable Inductive Biases and Physically Structured Learning, 2020. URL [https:
//inductive-biases.github.io/papers/31.pdf](https:
//inductive-biases.github.io/papers/31.pdf).

@inproceedings{lino2020simulating,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; title={{Simulating surface wave dynamics with convolutional networks}},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; author = {Mario Lino and Cantwell, Chris and Fotiadis, Stathi and Pignatelli, Eduardo and Bharath, Anil A.},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; booktitle={{NeurIPS Workshop on Interpretable Inductive Biases and Physically Structured Learning}},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; year={2020},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; url={https://inductive-biases.github.io/papers/31.pdf},<br>
}
