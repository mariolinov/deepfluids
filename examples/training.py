import torch
from torch import optim
from torchvision import transforms
from torch.utils.data import DataLoader
import deepfluids as df

save_info = {
	"root"    : "<Folder to save the output file with the weights>",
	"name"    : "<name of the output file>",
	"interval": 1,
}

core = 0
device = torch.device("cuda:"+str(core) if torch.cuda.is_available() else "cpu")
print("Using", device)

unet_hyperparams = df.learning.ConvNetHyperparams.from_dict({
    "kernel_size": 3,
    "stride": 1,
    "padding": 1,
    "pooling": 0,
    "input_size": (128, 128),
    "filters_dimensions": [64, 128, 256],
})
hyperparams = df.learning.Hyperparams.from_dict({
    "learning_rate": 0.0001,
		"batch_size": 32,	
		"n_frames_in": 5,
    "n_frames_out": 5,
    "n_frames_skip": 3,
})

model = df.models.UNet(hyperparams, unet_hyperparams, omega=True).to(device)
params = sum(p.numel() for p in model.parameters() if p.requires_grad)
print("Number of trainable parameters: ", params)

root = "/storage/deepx/SW/"
dataset1 = df.datasets.ShallowWater(root, hyperparams, keys=["Corner_Single_Drop"])
dataset1.transform = transforms.Compose([
    df.transforms.filter.Solid2Value(1),
    df.transforms.crop.Crop(15,(-0.2,0.2),(-0.2,0.2)),
    df.transforms.resize.Resize((128, 128)),
    df.transforms.convert.ToTensor(),
    df.transforms.normalise.Scale(1,1.1)
])
dataset2 = df.datasets.ShallowWater(root, hyperparams, keys=["Box_Single_Drop"])
dataset2.transform = transforms.Compose([
    df.transforms.filter.Solid2Value(1),
    df.transforms.resize.Resize((128, 128)),
    df.transforms.rotate.RandomRotate(90),
    df.transforms.rotate.RandomRotate(180),
    df.transforms.rotate.RandomRotate(270),
    df.transforms.flip.RandomHFlip(),
    df.transforms.flip.RandomVFlip(),
    df.transforms.convert.ToTensor(),
    df.transforms.normalise.Scale(1,1.1)
    ])

train_set  = dataset1  + dataset2
# Create DataLoader
train_loader = DataLoader(train_set, batch_size=hyperparams.batch_size, shuffle=True, drop_last=True, num_workers=4)


criterion = df.losses.GradLoss(g=0.05)
optimiser = torch.optim.Adam(model.parameters(), lr=hyperparams.learning_rate, weight_decay=1e-5)
scheduler = optim.lr_scheduler.MultiStepLR(optimiser, milestones=[100, 200, 300, 400], gamma=0.1)
model.fit(hyperparams, criterion, optimiser, train_loader, scheduler=scheduler, epochs=500, save_info=save_info, board=save_info["name"])
